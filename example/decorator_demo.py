import time

from NucleusAsync import async_worker


@async_worker
def test(arg):
    print('Start', arg)
    time.sleep(2)
    print('finish', arg)


test(0)
test(1)
