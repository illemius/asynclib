import random
import time

import sys

from NucleusAsync import async_call


def test(foo, bar):
    print('Start', foo, bar)
    time.sleep(2)
    print('Finish', foo, bar)
    return random.random()


def wrong_test(baz):
    print('Start', baz)
    time.sleep(1)
    raise Exception('Test error! ' + str(baz))


def error_handler():
    print('Exception: ', sys.exc_info()[1])


async_call(test, callback=lambda num: print('\t>', num), args=(0, 'test'))
async_call(test, args=(1, 'Hello world'))
async_call(wrong_test, on_error=error_handler, args=(2,))
