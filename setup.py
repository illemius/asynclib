from distutils.core import setup

setup(
    name='asynclib',
    version='0.1.0',
    packages=['NucleusAsync'],
    url='https://bitbucket.org/illemius/asynclib',
    license='MIT',
    author='jrootjunior',
    author_email='',
    description='Simple async'
)
