"""
Author: Alex Root Junior
https://bitbucket.org/illemius/asynclib
It part of Nucleus Frame
"""
import threading


def _async_engine(function: callable, callback: callable = None, on_error: callable = None, args: tuple = (),
                  kwargs: dict = None):
    if kwargs is None:
        kwargs = {}

    try:
        if not callable(function):
            raise TypeError('Function must be callable!')
        result = function(*args, **kwargs)
        if callable(callback):
            return callback(result)
    except:
        if callable(on_error):
            return on_error()
        raise


def async_call(function: callable, callback: callable = None, on_error: callable = None, args: tuple = (),
               kwargs: dict = None):
    """
    Asynchronously call function
    :param function: target function
    :param callback: which will return the function result
    :param on_error: error handler
    :param args: args for function
    :param kwargs: key-value args for function
    :return:
    """
    threading.Thread(target=_async_engine, args=(function, callback, on_error, args, kwargs)).start()


def async_worker(function):
    """
    Async decorator
    :param function:
    :return:
    """
    def wrapper(*args, **kwargs):
        threading.Thread(target=function, args=args, kwargs=kwargs).start()
    return wrapper
